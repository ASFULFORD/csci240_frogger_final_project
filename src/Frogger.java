import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.Random;

public class Frogger extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	//declaring shared variables
	int numOfCars = 28;
	int level = 1;
	int lives = 3;
	int score = 0;
	int carX = 0;
	int[][] tempArray = new int[7][7];
	Frog frog = null;
	Car[] car = null;
	JLabel levelLbl = new JLabel("Level:");
	JLabel levelNumLbl = new JLabel(String.valueOf(level));
	JLabel scoreLbl = new JLabel("Score:");
	JLabel scoreNumLbl = new JLabel(String.valueOf(score));
	JLabel livesLbl = new JLabel("Lives:");
	JLabel livesNumLbl = new JLabel(String.valueOf(lives));
	JButton newGame = new JButton("New Game");
	JButton upArrow = new JButton("\u2191");
	JButton downArrow = new JButton("\u2193");
	JButton leftArrow = new JButton("\u2190");
	JButton rightArrow = new JButton("\u2192");
	JPanel subPnlGameInfo = new JPanel();
	JPanel subPnlGameControls = new JPanel();
	JPanel subPnlNewGame = new JPanel();
	JPanel pnlControlsArea = new JPanel();
	JPanel pnlGameArea = new JPanel();
	GridBagConstraints c = new GridBagConstraints();
	Container main = this.getContentPane();
	ImageIcon greenFrog = createImageIcon("frog.png", "a frog");
	ImageIcon redCar = createImageIcon("car.png", "a car");
	ImageIcon greyRoad = createImageIcon("road.jpg", "a piece of road");
	ImageIcon greenGrass = createImageIcon("grass.jpg", "a piece of grass");
	
	//Frog class contains a position in x and y coords
	public class Frog
	{
		public int xCoord;
		public int yCoord;
		
		public Frog()
		{
			xCoord = 3;
			yCoord = 6;
		}
		public Frog(int x, int y)
		{
			xCoord = x;
			yCoord = y;
		}
		public void setXcoord(int x)
		{
			xCoord = x;
		}
		public int getXcoord()
		{
			return xCoord;
		}
		public void setYcoord(int y)
		{
			yCoord = y;
		}
		public int getYcoord()
		{
			return yCoord;
		}
	}//end Frog class
	//Car Class Contains position in x and y coords
	public class Car
	{
		public int xCoord;
		public int yCoord;
		
		public Car()
		{
			xCoord = -1;
			yCoord = 0;
		}
		public Car(int x, int y)
		{
			xCoord = x;
			yCoord = y;
		}
		public void setXcoord(int x)
		{
			xCoord = x;
		}
		public int getXcoord()
		{
			return xCoord;
		}
		public void setYcoord(int y)
		{
			yCoord = y;
		}
		public int getYcoord()
		{
			return yCoord;
		}
	}//end Car Class
	public static void main(String[] args)
	{
		new Frogger();
	}//end main
	public Frogger()
	{
		super("Turn-based Frogger");
		setupGame();
		setupGUI();
		registerListeners();
	}//end Frogger class
	public void setupGame()
	{
		//initialize frog
		frog = new Frog();
		//initialize all the cars needed
		car = new Car[numOfCars];
		for(int i = 0; i < numOfCars; i++)
		{
			//set to default coords (-1,0)
			car[i] = new Car();
		}
		//generate random cars
		generateRandomCars();
	}//end setupgame
	public void setupGUI()
	{
		//GUI setup
		subPnlGameInfo.setLayout(new GridLayout(3,3));
		subPnlGameInfo.setBackground(Color.WHITE);
		subPnlGameInfo.add(levelLbl);
		subPnlGameInfo.add(levelNumLbl);
		subPnlGameInfo.add(scoreLbl);
		subPnlGameInfo.add(scoreNumLbl);
		subPnlGameInfo.add(livesLbl);
		subPnlGameInfo.add(livesNumLbl);
		
		subPnlGameControls.setLayout(new GridBagLayout());
		subPnlGameControls.setBackground(Color.WHITE);
		c.gridx = 1;
		c.gridy = 0;
		subPnlGameControls.add(upArrow, c);
		c.gridx = 0;
		c.gridy = 1;
		subPnlGameControls.add(leftArrow, c);
		c.gridx = 1;
		c.gridy = 1;
		subPnlGameControls.add(downArrow, c);
		c.gridx = 2;
		c.gridy = 1;
		subPnlGameControls.add(rightArrow, c);
		
		subPnlNewGame.add(newGame);
		subPnlNewGame.setBackground(Color.WHITE);
		
		pnlControlsArea.setLayout(new GridLayout(3,1));
		pnlControlsArea.setBackground(Color.WHITE);
		pnlControlsArea.add(subPnlGameInfo);
		pnlControlsArea.add(subPnlGameControls);
		pnlControlsArea.add(subPnlNewGame);
		
		pnlGameArea.setLayout(new GridBagLayout());
		pnlGameArea.setBackground(Color.WHITE);
		
		//printing out the cars
		for(int k = 0; k < numOfCars; k++)
		{
			if(car[k].getXcoord() >= 0)
			{
				c.fill = GridBagConstraints.BOTH;
				c.gridx = car[k].getXcoord();
				c.gridy = car[k].getYcoord();
				c.weightx = 1.0;
				c.weighty = 1.0;
				c.ipadx = 1;
				c.ipady = 1;
				pnlGameArea.add(new JLabel(redCar), c);	
				//next line uses a temp array to keep track of xy-coords of generated cars
				tempArray[car[k].getXcoord()][car[k].getYcoord()] = 1;
			}//end if
		}//end for
		//printing out frog, bushes, and road
		for(int i = 0; i < 7; i++)
		{
			for(int j = 0; j < 7; j++)
			{
				if((i == frog.getXcoord() && j == frog.getYcoord()) && tempArray[i][j] !=1)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel(greenFrog), c);
				}//end if
				else if((j == 0 || j == 3 || j == 6) && tempArray[i][j] != 1)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel(greenGrass), c);
				}//end else if
				else
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel(greyRoad), c);
				}//end else
			}//end for
		}//end for
		//putting container together
		main.setLayout(new BorderLayout());
		main.add(pnlGameArea, BorderLayout.WEST);
		main.add(pnlControlsArea, BorderLayout.EAST);
		
		this.setSize(440,400);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}//end setupgui
	public void registerListeners()
	{
		newGame.addActionListener(this);
		upArrow.addActionListener(this);
		downArrow.addActionListener(this);
		leftArrow.addActionListener(this);
		rightArrow.addActionListener(this);
	}//end registerListeners
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == newGame)
		{
			newGame();
		}
		if(e.getSource() == upArrow)
		{
			moveUp();
		}
		if(e.getSource() == downArrow)
		{
			moveDown();
		}
		if(e.getSource() == leftArrow)
		{
			moveLeft();
		}
		if(e.getSource() == rightArrow)
		{
			moveRight();
		}
	}//end actionPerformed
	public void newGame()
	{
		//clears game
		this.dispose();
		//creates a new game
		new Frogger();
	}//end newgame
	public void moveUp()
	{
		//check to see if frog is at top
		if(frog.getYcoord() == 0)
		{
			//points for reaching top
			score += 500;
			//changing score
			scoreNumLbl.setText(String.valueOf(score));
			//resetting frog to starting position
			frog.setXcoord(3);
			frog.setYcoord(6);
			//increasing level
			level++;
			levelNumLbl.setText(String.valueOf(level));
			//move the cars on the board
			moveExistingCars();
			//randomly generate more cars
			generateRandomCars();
			//reprint the board
			printGameArea();
			//test whether or not the frog was hit
			ifHit();
		}//end if
		else
		{
			//points for moving up
			//change score
			score += 50;
			scoreNumLbl.setText(String.valueOf(score));
			//move frot up
			frog.setYcoord(frog.getYcoord() - 1);
			//move the cars on the board
			moveExistingCars();
			//randomly generate more cars
			generateRandomCars();
			//reprint board
			printGameArea();
			//test whether or not the for was hit
			ifHit();
		}//end else
	}//end moveup
	public void moveDown()
	{
		//see moveUp() for general idea
		if(frog.getYcoord() == 6)
		{
			moveExistingCars();
			generateRandomCars();
			printGameArea();
			ifHit();
		}//end if
		else
		{
			frog.setYcoord(frog.getYcoord() + 1);
			moveExistingCars();
			generateRandomCars();
			printGameArea();
			ifHit();
		}//end else
	}//end movedown
	public void moveLeft()
	{
		//see moveUp() for general idea
		if(frog.getXcoord() == 0)
		{
			moveExistingCars();
			generateRandomCars();
			printGameArea();
			ifHit();
		}//end if
		else
		{
			frog.setXcoord(frog.getXcoord() - 1);
			moveExistingCars();
			generateRandomCars();
			printGameArea();
			ifHit();
		}//end else
	}//end moveleft
	public void moveRight()
	{
		//see moveUp() for general idea
		if(frog.getXcoord() == 6)
		{
			moveExistingCars();
			generateRandomCars();
			printGameArea();
			ifHit();
		}//end if
		else
		{
			frog.setXcoord(frog.getXcoord() + 1);
			moveExistingCars();
			generateRandomCars();
			printGameArea();
			ifHit();
		}//end else
	}//end moveright
	public void printGameOver()
	{
		//prints GAME OVER when your out of lives
		pnlGameArea.removeAll();
		for(int i = 0; i < 7; i++)
		{
			for(int j = 0; j < 7; j++)
			{
				if(i == 1 && j == 2)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel("G"), c);
				}
				else if(i == 2 && j == 2)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel("A"), c);
				}
				else if(i == 3 && j == 2)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel("M"), c);
				}
				else if(i == 4 && j == 2)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel("E"), c);
				}
				else if(i == 2 && j == 4)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel("O"), c);
				}
				else if(i == 3 && j == 4)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel("V"), c);
				}
				else if(i == 4 && j == 4)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel("E"), c);
				}
				else if(i == 5 && j == 4)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel("R"), c);
				}
				else
				{
					c.fill = GridBagConstraints.CENTER;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel(greenGrass), c);
				}
			}
		}
		pnlGameArea.validate();
		pnlGameArea.repaint();
	}
	public void ifHit()
	{
		//iterate through array of cars
		for(int i = 0; i < numOfCars; i++)
		{
			//checks if the frogs coords match the cars coords
			if(frog.getXcoord() == car[i].getXcoord() && frog.getYcoord() == car[i].getYcoord())
			{
				//if so, move frog to starting position
				frog.setXcoord(3);
				frog.setYcoord(6);
				//reduce lives
				lives--;
				livesNumLbl.setText(String.valueOf(lives));
				//move cars on board
				moveExistingCars();
				//generate random cars
				generateRandomCars();
				//reprint board
				printGameArea();
			}//end if
		}//end for
		//if lives = 0, game over, bro!
		if(lives == 0)
		{
			printGameOver();
		}//end if
	}//end ifhit
	public void generateRandomCars()
	{
		Random rand = new Random();
		int n = 0, numOfCarsInserted = 0;
		//randomly inserts cars into starting positions for lane 1 and 2
		//while less than 2 cars are inserted
		while(numOfCarsInserted < 2)
		{
			//iterate through car array
			for(int i = 0; i < numOfCars; i++)
			{
				//if car's position is -1
				if(car[i].getXcoord() < 0)
				{
					//generate random number
					n = rand.nextInt(100) + 1;
					//check its value against a number that is determined by level
					if(n > 82 - (level * 2))
					{
						//if so, generate car in lane 1
						car[i].setXcoord(0);
						car[i].setYcoord(1 + numOfCarsInserted);
						//increase variable
						numOfCarsInserted++;
					}//end if
					else
					{
						//else: if number not high enough, no car added, but increase variable
						numOfCarsInserted++;
					}//end else
					//this makes sure that only 2 attempts to insert cars are made: for lane 1 and for lane 2
					if(numOfCarsInserted == 2)
					{
						break;
					}//end if
				}//end if
			}//end for
		}//end while
		
		//randomly inserts cars into starting positions for lane 4 and 5
		//same as previous while loop.  Only subtle differences regarding lane position
		numOfCarsInserted = 0;
		while(numOfCarsInserted < 2)
		{
			for(int i = 0; i < numOfCars; i++)
			{
				if(car[i].getXcoord() < 0)
				{
					n = rand.nextInt(100) + 1;
					if(n > 52 - (level * 2))
					{
						car[i].setXcoord(0);
						car[i].setYcoord(4 + numOfCarsInserted);
						numOfCarsInserted++;
					}//end if
					else
					{
						numOfCarsInserted++;
					}//end else
					if(numOfCarsInserted == 2)
					{
						break;
					}//end if
				}//end if
			}//end for
		}//end while
	}//end generaterandomecar
	public void moveExistingCars()
	{
		//iterate through car array
		for(int i = 0; i < numOfCars; i++)
		{
			//if a car is in the element, it will have a non negative number in its x-coord
			if(car[i].getXcoord() >= 0)
			{
				//if so, set the x-coord one space to the right
				carX = car[i].getXcoord() + 1;
				car[i].setXcoord(carX);
			}//end if
			//test whether or not car is at the far right
			if(car[i].getXcoord() == 7)
			{
				//if so, set the car's x-coord to -1, so that it can be used by random generateRandomCars()
				car[i].setXcoord(-1);
				car[i].setYcoord(0);
			}//end if
		}//end for
	}//end moveexistingcars
	public void printGameArea()
	{
		//set tempArray back to all zeros
		for(int i = 0; i < 7; i++)
			{
				for(int j = 0; j < 7; j++)
				{
					tempArray[i][j] = 0;
				}
			}
		//remove all components from gameboard
		pnlGameArea.removeAll();
		//please refer to setupGUI() for comments on reprinting the gameboard
		for(int k = 0; k < numOfCars; k++)
		{
			if(car[k].getXcoord() >= 0)
			{
				c.fill = GridBagConstraints.BOTH;
				c.gridx = car[k].getXcoord();
				c.gridy = car[k].getYcoord();
				c.weightx = 1.0;
				c.weighty = 1.0;
				c.ipadx = 1;
				c.ipady = 1;
				pnlGameArea.add(new JLabel(redCar), c);	
				tempArray[car[k].getXcoord()][car[k].getYcoord()] = 1;
			}
		}
		
		for(int i = 0; i < 7; i++)
		{
			for(int j = 0; j < 7; j++)
			{
				if((i == frog.getXcoord() && j == frog.getYcoord()) && tempArray[i][j] !=1)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel(greenFrog), c);
				}
				else if((j == 0 || j == 3 || j == 6) && tempArray[i][j] != 1)
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel(greenGrass), c);
				}
				else
				{
					c.fill = GridBagConstraints.BOTH;
					c.gridx = i;
					c.gridy = j;
					c.weightx = 1.0;
					c.weighty = 1.0;
					c.ipadx = 1;
					c.ipady = 1;
					pnlGameArea.add(new JLabel(greyRoad), c);
				}
			}
		}
		//validate and repaint the gameboard
		main.validate();
		pnlGameArea.repaint();
	}
	protected ImageIcon createImageIcon(String path, String description)
	{
		//this function turns the selected image into icons for the JLabels
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null)
		{
			return new ImageIcon(imgURL, description);
		}//end if
		else 
		{
			System.err.println("Couldn't find file: " + path);
			return null;
		}//end else
	}//end createimageicon
}//end Frogger Class